# My Seminar Slides for the DimSum Paper

My slides, using https://sli.dev/,
about the [DimSum paper](https://plv.mpi-sws.org/dimsum/)
for the ETH Computer Science Seminar in [Research Topics in Software Engineering](https://pls.inf.ethz.ch/education/Research_Topics_in_Software_Engineering.html) HS23.

## Installation
- see https://sli.dev/guide/install.html for how-to install Slidev
    - `sudo npm i -g @slidev/cli @slidev/theme-default` to install slidev and a theme globally
    - `npm i @slidev/types` to install types module locally
        - not sure why global install does not work

## Usage
- run `npx slidev slides.md` in the repositories root directory

## Authors and acknowledgment

Paper by: Michael Sammler, Simon Spies, Youngju Song, Emanuele D'Osualdo, Robbert Krebbers, Deepak Garg, Derek Dreyer. 
See: https://plv.mpi-sws.org/dimsum/
