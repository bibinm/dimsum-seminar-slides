---
info: |
  ## DimSum Seminar
  
  Seminar Presentation about the Paper "DimSum: A Decentralized Approach to Multi-language Semantics and Verification"
  for the ETH Computer Science Seminar in [Research Topics in Software Engineering](https://pls.inf.ethz.ch/education/Research_Topics_in_Software_Engineering.html) in HS23.

  - [Website of the Paper](https://plv.mpi-sws.org/dimsum/)
  - [Paper](https://plv.mpi-sws.org/dimsum/paper.pdf)
  - [Presentation Source](https://gitlab.ethz.ch/bibinm/dimsum-seminar-slides)
  - Created using [Sli.dev](https://sli.dev)

  Presented by [Bibin Muttappillil](bibinm@student.ethz.ch)
title: DimSum
background: /dumplings.jpg
download: true
---

<h2 style="background-color: sienna;"><b>DimSum: A Decentralized Approach to Multi-language Semantics and Verification</b></h2>

Bibin Muttappillil

<footer class="absolute bottom-0 right-2 slide-number">Picture from https://pixabay.com/photos/dumplings-dimsum-appetizer-cuisine-5180428/</footer>

<!--
The last comment block of each slide will be treated as slide notes. It will be visible and editable in Presenter Mode along with the slide. [Read more in the docs](https://sli.dev/guide/syntax.html#notes)
-->
---

<div v-click="2">

# A Decentralized Approach to Multi-language Semantics and <span style="color:blue">*Verification*</span>

</div>

<div v-click="1">

<Transform :scale="5">

🚀 ⇝ 💥

</Transform>

</div>

<!--
Background: Verification
-->
---

# A Decentralized Approach to <span style="color:blue">*Multi-language*</span> Semantics and Verification


<v-clicks depth="1">

- common assumption: *one* well-defined language

- problem:
    - Go, OCaml, Python, Rust, JavaScript, C++, ...
    - libraries in C or Assembly using SysCalls ...

- language differences
    - function calls vs jumps
    - representation of values (Objects?)
    - memory model
    - language features

</v-clicks>

<!--

- common assumption in verification: *one* well defined language to verify
- problem: real-world programs are assembled from multiple languages
    - diverse languages: Go, OCaml, Python, Rust, Javascript, C++, ...
    - high-level languages using libraries in C, Assembly, SysCalls, ...
- not enough to reason components separately
    - interactions between them
    - boundaries
    - language differences
        - function calls vs jumps
        - representation of values (Objects?)
        - memory model
        - language features (control-flow)
-->

---

# A Decentralized Approach to Multi-language <span style="color:blue">*Semantics*</span> and Verification


<v-clicks depth="1">

- Semantic vs Syntax
    - ```cpp
      5 / 2  // 2 in C++
      ```
    - ```py
      5 / 2  # 2.5 Python
      ```

- Motivation: right way to define behavior and interoperation of multi-language programs?

</v-clicks>

<!--
-->

---

# A <span style="color:blue">*Decentralized*</span> Approach to Multi-language Semantics and Verification

<v-clicks depth="2">

- semantics are *not* fixed up-front
- semantics of a library can be specified
    - without knowing what language or memory model of other libraries
- 4 principles
    - no fixed source language
    - no fixed set of (interoperating) languages
        - no syntactic multi-language
    - no fixed memory model
        - pointer: block ids with an offset vs integer addresses
    - no fixed notion of linking
        - coroutine linking vs function calls

</v-clicks>

<!--
if I have a C library, I do not want to write a specification in Assembly
if I have an Asm library, I may not be able to write a specification in C

- No fixed source language
    - allow linking of high-level code with low-level libraries that have no high-level semantic equivalent
    - do not use lowest language
    - so no 'source' language is fixed

- No fixed set of (interoperating) languages
    - language-local reasoning
    - should be able to verify a library in one language
        - without worrying *a priori* about other language that library could be linked with
    - other approaches used a syntactic multi-language
        - a proof of a library in one language must take all other languages into account

- No fixed memory model
    - support linking of libraries in languages with different memory models
        - linking should be able to be reasoned about in a language-local way
    - our example will use link
        - an abstract memory model, pointer are block ids with an offset
        - a concrete memory model, pointers are integer addresses

- No fixed notion of linking
    - avoid fixing any "official" notion of semantic linking up front
    - instead: users of the framework develop new library-specific notion of linking
        - that support higher-level reasoning principles
    - the `yield` examples shows
        - that we only care about the high-level that the coroutine, a function call is a return for someone else
        - we can verify `yield` in Asm
        - then we can use this coroutine linking a the language-level reasoning of Rec
            - so we do not have to reason about the compiled Asm of Rec
                - which saves effort
-->

$$
\newcommand{\AsmCol}[1]{ {\color{blue} #1} }
\newcommand{\RecCol}[1]{ {\color{tomato} #1} }
\newcommand{\SpecCol}[1]{ {\color{black} #1} }

\newcommand{\AsmMod}[1]{ \AsmCol{\texttt{#1}} }
\newcommand{\RecMod}[1]{ \RecCol{\texttt{#1}} }

\newcommand{\AsmSynLink}{ \; \AsmCol{\cup_a} \; }
\newcommand{\RecSynLink}{ \; \RecCol{\cup_r} \; }

\newcommand{\Compile}{ \downarrow }

\newcommand{\Sem}[3]{ #1{\llbracket} #3 #1{\rrbracket_{#2}} }
\newcommand{\AsmSem}[1]{ \Sem{\AsmCol}{a}{#1} }
\newcommand{\RecSem}[1]{ \Sem{\RecCol}{r}{#1} }
\newcommand{\SpecSem}[1]{ \Sem{\SpecCol}{s}{#1} }

\newcommand{\AsmSemLink}{ \; \AsmCol{\oplus_a} \; }
\newcommand{\RecSemLink}{ \; \RecCol{\oplus_r} \; }

\newcommand{\WrapR}{ \rceil_{\RecCol{r} \rightleftharpoons \AsmCol{a}} }
\newcommand{\Wrap}[1]{ \lceil #1 \WrapR }

\newcommand{\LineZero}{
    \Compile \RecMod{main} \AsmSynLink \Compile \RecMod{memmove} \AsmSynLink \AsmMod{locle} \AsmSynLink \AsmMod{print}
}

\newcommand{\LineOne}{ = \AsmSem{ \LineZero } }
$$

---

# Example Problem

<v-clicks>

```rust
// Fictional Rec language with C-like memory model
fn main() = let x[3] = {1, 2, 0};
            memmove(x+1, x+0, 2);  // x -> [1, 1, 2]
            print(x[1]); print(x[2]);

```

```rust
// memmove library in Rec
fn memmove(d, s, n) = if locle(d, s): memcpy(d, s, n, 1) else: memcpy(d+n-1, s+n-1, n, -1)

fn memcpy(d, s, n, o) = if o < n: d <- !s; memcpy(d, s, n, 1)
```

```asmatmel
# locle library in Asm resembling Assembly
locle:  sle x0, x0, x1
        ret
```

```asmatmel
# print library in Asm
print:  mov x8, PRINT;
        syscall;
        ret
```

Goal: verify 

$\AsmMod{onetwo} \triangleq \; \LineZero$

</v-clicks>

<!--
What is the Goal? Multi-Language Verification between
- very different language
    - high and low level
    - memory model
    - mainly written in a high-level language
    - using high-level and low-level libraries

- with user defined specification in specification language
- user is allowed to reason on the level of the high-level language
    - not just the assembled program

Explain
- Compile: from Rec to Asm
- Syntactic Asm Linking


Go over the proof if §2
- go over the 8 refinements and show their respective proof strategy
    - explaining the general proof strategy that should work for other programs as well

# Paper

- new approach zo multi-language semantics and verification
    - decentralized according to the 4 principles from before
- DimSum
    - Coq-based framework

<!--
- What is Coq?
Do I need to mention DimSum and Coq now already?
-->
---

# Using *Modules* for decentralized multi-language semantics

<v-clicks>

- semantics of a library
    - 'module' := labeled transition system
- each language has a set of events
    - $\AsmMod{Jump}$ for $\AsmMod{Asm}$ and $\RecMod{Call}$ or $\RecMod{Return}$ for $\RecMod{Rec}$
- interaction of modules as synchronization on events
    - out-jumps are synced with in-jumps
    - $\AsmMod{Jump?(r, m)}$ is the accepting event of $\AsmMod{Jump!(r, m)}$ of another module
- the events carry a detailed description of the program state
    - $\AsmMod{Jump!(r, m)}$, carrying register and memory state
    - $\RecMod{Call!(f, v, m)}$, carrying function name, function arguments, and memory state
    - $\RecMod{Return!(v, m)}$

</v-clicks>

<!--

- basis for all their reasonings
- starting point: work around CompCert

# Modules as communicating processes

- explain **events**: core of the whole operation

- Open-world events (ideas from literature)
    - visible parts of the program state included in the events
    - modules share state
-->

---
layout: two-cols-header
---

# Using *Modules* for decentralized multi-language semantics


::left::

```asmatmel
# print library in Asm
print:  mov x8, PRINT;
        syscall;
        ret
```

<Transform :scale="0.6">
<v-clicks>

```mermaid
stateDiagram

    Wait: Wait, A
    WaitSys: WaitSys(r'), A
    move: Run(r, m), A
    syscall: Run(r', m), A
    ret: Run(r'', m), A

    Wait --> move: Jump?(r, m)
    move --> syscall: τ
    syscall --> WaitSys: Jump!(r'', m)
    WaitSys --> ret: Jump?(r'', m)
    ret --> Wait: Jump!(r''', m)
```

</v-clicks>
</Transform>

::right::

<!--
-->

---

# Proof Outline 1

(example specific)

$$
\newcommand{\LineTwo}{
    \preceq \AsmSem{\Compile \RecMod{main}} \AsmSemLink \AsmSem{\Compile \RecMod{memmove}} \AsmSemLink \AsmSem{\AsmMod{locle}} \AsmSemLink \AsmSem{\AsmMod{print}}
}

\newcommand{\LineThree}{
    \preceq \Wrap{\RecSem{\RecMod{main}}} \AsmSemLink \Wrap{\RecSem{\RecMod{memmove}}} \AsmSemLink \AsmSem{\AsmMod{locle}} \AsmSemLink \AsmSem{\AsmMod{print}}
}

\newcommand{\LineFour}{
    \preceq \Wrap{\RecSem{\RecMod{main}}} \AsmSemLink \Wrap{\RecSem{\RecMod{memmove}}} \AsmSemLink \Wrap{\SpecSem{\RecMod{locle}_{spec}}} \AsmSemLink \SpecSem{\AsmMod{print}_{spec}}
}

\newcommand{\LineFive}{
    \preceq \Wrap{\RecSem{\RecMod{main}} \RecSemLink \RecSem{\RecMod{memmove}} \RecSemLink \SpecSem{\RecMod{locle}_{spec}}} \AsmSemLink \SpecSem{\AsmMod{print}_{spec}}
}

\newcommand{\LineSix}{
    \preceq \Wrap{\RecSem{\RecMod{main} \RecSynLink \RecMod{memmove}} \RecSemLink \SpecSem{\RecMod{locle}_{spec}}} \AsmSemLink \SpecSem{\AsmMod{print}_{spec}}
}

\newcommand{\LineSeven}{
    \preceq \Wrap{\SpecSem{\RecMod{main}_{spec}}} \AsmSemLink \SpecSem{\AsmMod{print}_{spec}}
}

\newcommand{\LineEight}{
    \preceq \SpecSem{\AsmMod{onetwo}_{spec}}
}
$$

$\AsmSem{\AsmMod{onetwo}}$

$\LineOne$

&ZeroWidthSpace;

&ZeroWidthSpace;

&ZeroWidthSpace;

&ZeroWidthSpace;

&ZeroWidthSpace;

&ZeroWidthSpace;

$\LineEight$

<!--

$\AsmMod{onetwo} \triangleq \; \LineZero$

$\AsmSem{\AsmMod{onetwo}} \LineOne$

- notation: $\AsmSem{\AsmMod{print}}$, $\RecSem{\RecMod{main}}$
-->

---

# Proof Outline 1 → 2

(general)

<old>

$\AsmSem{\AsmMod{onetwo}}$

</old>

$= \AsmCol{\llbracket}$<oof>
$\; \Compile \RecMod{main}$</oof>
$\AsmSynLink$<oof>
$\; \Compile \RecMod{memmove}$</oof>
$\AsmSynLink$<oof>
$\AsmMod{locle}$</oof>
$\AsmSynLink$<oof>
$\AsmMod{print}$</oof>
$\AsmCol{\rrbracket_a}$


$\preceq \AsmCol{\llbracket}$<oof>
$\; \Compile \RecMod{main}$</oof>
$\AsmCol{\rrbracket_a} \AsmSemLink \AsmCol{\llbracket}$<oof>
$\; \Compile \RecMod{memmove}$</oof>
$\AsmCol{\rrbracket_a} \AsmSemLink \AsmCol{\llbracket}$<oof>
$\AsmMod{locle}$</oof>
$\AsmCol{\rrbracket_a} \AsmSemLink \AsmCol{\llbracket}$<oof>
$\AsmMod{print}$</oof>
$\AsmCol{\rrbracket_a}$

<div class="absolute bottom-10 right-30">

<v-clicks>

in "general": $\AsmSem{\AsmMod{A} \AsmSynLink \AsmMod{A'}} \equiv \AsmSem{\AsmMod{A}} \AsmSemLink \AsmSem{\AsmMod{A'}}$

$\equiv \text{is} \preceq \text{and} \succeq$

$\preceq$ refinement (later)

</v-clicks>

</div>

<!--
Correct as long as addresses do not overlap
-->

---

# Proof Outline 2

&ZeroWidthSpace;

<old>

$\AsmSem{\AsmMod{onetwo}}$

$\LineOne$

</old>

$\LineTwo$

---

# Proof Outline 2 → 3

(general)

<old>

$\AsmSem{\AsmMod{onetwo}}$

$\LineOne$

</old>

$\preceq \AsmSem{\Compile \RecMod{main}}$<oof>
$\AsmSemLink$</oof>
$\AsmSem{\Compile \RecMod{memmove}}$<oof>
$\AsmSemLink \AsmSem{\AsmMod{locle}} \AsmSemLink \AsmSem{\AsmMod{print}}$</oof>

$\preceq \Wrap{\RecSem{\RecMod{main}}}$<oof>
$\AsmSemLink$</oof>
$\Wrap{\RecSem{\RecMod{memmove}}}$<oof>
$\AsmSemLink \AsmSem{\AsmMod{locle}} \AsmSemLink \AsmSem{\AsmMod{print}}$</oof>

<div class="absolute bottom-10 right-30">

<v-clicks>

$\Wrap{\RecMod{.}}$ is an embedding of $\RecMod{Rec}$ Modules into $\AsmMod{Asm}$

<img src="/wrap_event_example.png" class="h-20 rounded shadow" />



in general: $\AsmSem{ \Compile \RecMod{R}} \preceq \Wrap{\RecMod{R}}$

</v-clicks>

</div>

<footer class="absolute bottom-0 right-2 slide-number">Picture from copied from the DimSum Paper</footer>

<!--
Compiler should be correct up to specification
-->

---

# Proof Outline 3

&ZeroWidthSpace;

<old>

$\AsmSem{\AsmMod{onetwo}}$

$\LineOne$

$\LineTwo$

</old>

$\LineThree$

---

# Proof Outline 3 → 4

(example specific)

<old>

$\AsmSem{\AsmMod{onetwo}}$

$\LineOne$

$\LineTwo$

</old>

&ZeroWidthSpace;<oof>
$\preceq \Wrap{\RecSem{\RecMod{main}}} \AsmSemLink \Wrap{\RecSem{\RecMod{memmove}}} \AsmSemLink$</oof>
$\AsmSem{\AsmMod{locle}}$<oof>
$\AsmSemLink$</oof>
$\AsmSem{\AsmMod{print}}$

&ZeroWidthSpace;<oof>
$\preceq \Wrap{\RecSem{\RecMod{main}}} \AsmSemLink \Wrap{\RecSem{\RecMod{memmove}}} \AsmSemLink$</oof>
$\Wrap{\SpecSem{\RecMod{locle}_{spec}}}$<oof>
$\AsmSemLink$</oof>
$\SpecSem{\AsmMod{print}_{spec}}$

<div class="absolute bottom-10 right-30">

<v-click>

```rust
// "simplified specification" for locle
if a.blockid == b.blockid:
    return a.offset < b.offset
else:
    return ?
```

</v-click>

</div>

---

# Proof Outline 4

&ZeroWidthSpace;

<old>

$\AsmSem{\AsmMod{onetwo}}$

$\LineOne$

$\LineTwo$

$\LineThree$

</old>

$\LineFour$

---

# Proof Outline 4 → 5

(in general)

<old>

$\AsmSem{\AsmMod{onetwo}}$

$\LineOne$

$\LineTwo$

$\LineThree$

</old>

$\preceq \Wrap{\RecSem{\RecMod{main}}} \AsmSemLink \Wrap{\RecSem{\RecMod{memmove}}} \AsmSemLink \Wrap{\SpecSem{\RecMod{locle}_{spec}}}$<oof>
$\AsmSemLink \SpecSem{\AsmMod{print}_{spec}}$</oof>

$\preceq \Wrap{\RecSem{\RecMod{main}} \RecSemLink \RecSem{\RecMod{memmove}} \RecSemLink \SpecSem{\RecMod{locle}_{spec}}}$<oof>
$\AsmSemLink \SpecSem{\AsmMod{print}_{spec}}$</oof>

<div class="absolute bottom-10 right-30">

<v-click>

general rule:

$\Wrap{\RecMod{M}} \AsmSemLink \Wrap{\RecMod{M'}} \preceq \Wrap{\RecMod{M} \RecSemLink \RecMod{M'}}$

</v-click>

</div>

---

# Proof Outline 5

&ZeroWidthSpace;

<old>

$\AsmSem{\AsmMod{onetwo}}$

$\LineOne$

$\LineTwo$

$\LineThree$

$\LineFour$

</old>

$\LineFive$

---

# Proof Outline 5 → 6

(in general)

<old>

$\AsmSem{\AsmMod{onetwo}}$

$\LineOne$

$\LineTwo$

$\LineThree$

$\LineFour$

</old>


&ZeroWidthSpace;<oof>
$\preceq \lceil$</oof>
$\RecSem{\RecMod{main}} \RecSemLink \RecSem{\RecMod{memmove}}$<oof>
$\RecSemLink \SpecSem{\RecMod{locle}_{spec}} \WrapR \AsmSemLink \SpecSem{\AsmMod{print}_{spec}}$</oof>


&ZeroWidthSpace;<oof>
$\preceq \lceil$</oof>
$\RecSem{\RecMod{main} \RecSynLink \RecMod{memmove}}$<oof>
$\RecSemLink \SpecSem{\RecMod{locle}_{spec}} \WrapR \AsmSemLink \SpecSem{\AsmMod{print}_{spec}}$</oof>

<div class="absolute bottom-10 right-30">

<v-clicks>

semantic to syntactic

</v-clicks>

</div>

<!--
decentralized advanteage
- when we reason about a set of modules
    - we only need to care about event and language to which these events belong to
    - reasoning about two Rec?
        - no need to look at calling conventions or memory model of Asm

-->

---

# Proof Outline 6

&ZeroWidthSpace;

<old>

$\AsmSem{\AsmMod{onetwo}}$

$\LineOne$

$\LineTwo$

$\LineThree$

$\LineFour$

$\LineFive$

</old>

$\LineSix$

---

# Proof Outline 6 → 7

(example specific)

<old>

$\AsmSem{\AsmMod{onetwo}}$

$\LineOne$

$\LineTwo$

$\LineThree$

$\LineFour$

$\LineFive$

</old>


&ZeroWidthSpace;<oof>
$\preceq \lceil$</oof>
$\RecSem{\RecMod{main} \RecSynLink \RecMod{memmove}} \RecSemLink \SpecSem{\RecMod{locle}_{spec}}$<oof>
$\WrapR \AsmSemLink \SpecSem{\AsmMod{print}_{spec}}$</oof>

&ZeroWidthSpace;<oof>
$\preceq \lceil$</oof>
$\SpecSem{\RecMod{main}_{spec}}$<oof>
$\WrapR \AsmSemLink \SpecSem{\AsmMod{print}_{spec}}$</oof>

<div class="absolute bottom-10 right-30">

<v-clicks>

reasoning on $\RecCol{Rec}$ level

</v-clicks>

</div>

---

# Proof Outline 7

&ZeroWidthSpace;

<old>

$\AsmSem{\AsmMod{onetwo}}$

$\LineOne$

$\LineTwo$

$\LineThree$

$\LineFour$

$\LineFive$

$\LineSix$

</old>

$\LineSeven$

---

# Proof Outline 7 → 8

(example specific)

<old>

$\AsmSem{\AsmMod{onetwo}}$

$\LineOne$

$\LineTwo$

$\LineThree$

$\LineFour$

$\LineFive$

$\LineSix$

</old>

$\LineSeven$

$\LineEight$



<div class="absolute bottom-10 right-30">

<v-clicks>

syscalls: reasoning on Asm level

</v-clicks>

</div>

<!--
-->

---

# Proof Outline All

&ZeroWidthSpace;

$\AsmSem{\AsmMod{onetwo}}$

$\LineOne$

$\LineTwo$

$\LineThree$

$\LineFour$

$\LineFive$

$\LineSix$

$\LineSeven$

$\LineEight$

---

# Wrapper $\Wrap{\RecMod{.}}$

<v-clicks>

- semantic wrappers
- translations operation on modules
    - translating events between two languages

- <img src="/wrap_event_example.png" class="h-20 rounded shadow" />

- Kripke relations

- Angelic non-determinism

</v-clicks>


<footer class="absolute bottom-0 right-2 slide-number">Picture from copied from the DimSum Paper</footer>

<!--

<!--
- semantic wrappers
    - (ideas from literature): (syntactic was used in other literature to express expression of on language in the syntactic multi-language)


-->

---

# Wrapper $\Wrap{\RecMod{.}}$: $\RecMod{Rec}$ to $\AsmMod{Asm}$ with Kripke relations

<v-clicks depth="2">

- high-level with more structure to low-level
    - $42 \sim_w 42$
    - $true \sim_w 1$
    - $arr[5] \sim_w w(arr) + 5$
- challenge: consistent block-id mapping

- Kripke Relations: relations that maintain evolving internal state

- solution: $\Wrap{}$ keeps a block-id-to-address map $w$
    - keeps growing as $\Wrap{}$ executes

</v-clicks>

<!--
Kripke Relations
- relations that maintain evolving internal state
    - e.g. track relationship between growing heaps
- Wrappers have the role of expressive simulation
    - use Kripke Relations to define them

In the full definition of the wrapper, this piece of state 𝑤 is maintained
using a separation logic relation (akin to the relations in §5).
In the simplified account of ⌈·⌉r⇌a that we discuss here, one can think of the map 𝑤 as one component of the internal state of the wrapper.
-->

---

# Wrapper $\Wrap{\RecMod{.}}$: $\AsmMod{Asm}$ to $\RecMod{Rec}$ Angelic non-determinism

<v-clicks depth="2">

- problem: $Return?(0, m)$ in $\AsmMod{Asm}$ to $?$ in $\RecMod{Rec}$
    - $0$
    - $false$
    - $null$
- solution: angelic non-determinism
    - an angel will pick the correct translation
    - translate to all of them
    - continue for all possible states
- refinement with angelic non-determenism
    - how does refinement ($\preceq$) work if we continue in all possible states?
    - our definition of ($\preceq$) will allow for angelic non-determenism
    - Blackboard Time!

</v-clicks>

<!--

- angelic and demonic non-determinism used for rely-guarantee reasoning between
    - a program component and
    - its environment

- (ideas from literature) other literature: used this for user-provided pre- and post-conditions for contextual refinement
- DimSum: apply in our wrappers
        - define language-specific protocols between
            - module and
            - its environment
-->

---

# Paper

<v-clicks depth="2">

- generalizes combinators $\oplus$, $\lceil \rceil_X$, $\setminus$, $\times$
    - language-agnostic
        - instantiate for each language
    - most properties hold

- DimSum: Coq-based framework (digital interactive proof assistant)

- additional example: coroutine linking

</v-clicks>

<!--
- Coq-based framework (digital interactive proof assistant)
-->

---
layout: two-cols-header
---

# Evaluation

<v-clicks depth="2">

- published 2023 Jan
- a first step in exploring decentralized approach for multi-language verification

</v-clicks>

::left::

### Positives

<v-clicks depth="2">

+ old proofs work when adding a new language
+ ability to reason on a higher level language
+ can verify library, not only whole programs
+ no assumption on memory model or linking

</v-clicks>


::right::

### (Limitations)

<v-clicks depth="2">

- only applied to Rec, Asm, Spec
    - significantly simpler than realistic languages
- language features
    - closure, garbage collection, types, concurrency

- only safety properties, no liveness property

- manual verification, not automated

</v-clicks>

<!--

- sounds cumbersome: users of the framework develop new library-specific notion of linking

- work: have to port the language and events into DimSum

- Their comparison to other approaches in §6

-->

---

# Summary

<v-clicks depth="2">

- DimSum: Coq-based framework 
- semantic notion of
    - module & refinement
    - library of language-agnostic combinators
        - linking modules
        - translating modules (Kripke Relations & Angelic non-determenism)
- verification rules
- multi-language & decentralized (4 principles)
- applied to
    - Rec, Asm, Spec, with linking operators and wrappers
    - two Asm libraries extending functionality of Rec
    - multi-pass compiler from Rec to Asm

</v-clicks>


<!--

## Related work?

- CompCert

-->

---
layout: statement
---

# Thanks for Listening!

<v-click>

<Transform :scale="7">
<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path fill="currentColor" d="M12 3C6.5 3 2 6.6 2 11c0 2.1 1 4.1 2.8 5.5c0 .6-.4 2.2-2.8 4.5c0 0 3.5 0 6.5-2.5c1.1.3 2.3.5 3.5.5c5.5 0 10-3.6 10-8s-4.5-8-10-8m1 12h-2v-2h2v2m1.8-5c-.3.4-.7.6-1.1.8c-.3.2-.4.3-.5.5c-.2.2-.2.4-.2.7h-2c0-.5.1-.8.3-1.1c.2-.2.6-.5 1.1-.8c.3-.1.5-.3.6-.5c.1-.2.2-.5.2-.7c0-.3-.1-.5-.3-.7c-.2-.2-.5-.3-.8-.3c-.3 0-.5.1-.7.2c-.2.1-.3.3-.3.6h-2c.1-.7.4-1.3.9-1.7c.5-.4 1.2-.5 2.1-.5c.9 0 1.7.2 2.2.6c.5.4.8 1 .8 1.7c.1.4 0 .8-.3 1.2Z"/></svg>
</Transform>


</v-click>

<footer class="absolute bottom-0 right-2 slide-number">Slides can be found at https://gitlab.ethz.ch/bibinm/dimsum-seminar-slides</footer>

