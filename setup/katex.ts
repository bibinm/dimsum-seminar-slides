import { defineKatexSetup } from '@slidev/types'

const macros = {};

export default defineKatexSetup(() => {
  return {
    globalGroup: true,
    macros,
  }
})

